public class VilsAppTriggerHandler {
	public static void afterinsert(Map<Id,VILS_Application__c> newMap){
		
		List<String> nameList = new List<String>();
		List<String> billingStateList = new List<String>();
		Map<String,VILS_Application__c> vAppMap = new Map<String,VILS_Application__c>();
		List<VILS_Application__c> updateList = new List<VILS_Application__c>();
		List<Account> insertLIst = new List<Account>();
		try{
			for(VILS_Application__c vApp: [Select Id,District_Name_Text__c,District_State__c FROM VILS_Application__c where Id IN :Trigger.newMap.keySet()]){
				nameList.add(vApp.District_Name_Text__c);
				billingStateList.add(vApp.District_State__c);
				vAppMap.put(vApp.District_Name_Text__c+'::'+vApp.District_State__c,vApp);
			}

			for(Account acc:[Select Id,Name,BillingState from Account where Name in :nameList or BillingState in :billingStateList]){
				if(vAppMap.containsKey(acc.Name+'::'+acc.BillingState)){
					VILS_Application__c vApp= vAppMap.get(acc.Name+'::'+acc.BillingState);
					vApp.District_Name__c =acc.Id;
					updateList.add(vApp);
					vAppMap.remove(acc.Name+'::'+acc.BillingState);
				}
			}
            //
            for(VILS_Application__c vApp:vAppMap.values()){
            	Account acc = new Account();
            	acc.Name=vApp.District_Name_Text__c;
            	acc.BillingState= vApp.District_State__c;
            	insertList.add(acc);
            }

            if(insertList.size()>0){
            	insert insertList;
            }
            for(Account acc: insertList){
            	VILS_Application__c vApp= vAppMap.get(acc.Name+'::'+acc.BillingState);
            	vApp.District_Name__c =acc.Id;
            	updateList.add(vApp);
            	vAppMap.remove(acc.Name+'::'+acc.BillingState);
            }

            if(updateList.size()>0){
            	update updateList;
            }
        }catch(exception e)
        {
        	system.debug(e);
        }

    }

    public static	void updateContact(Map<Id,VILS_Application__c> newMap){

        List<String> cnameList = new List<String>();
        List<String> emailList = new List<String>();
        Map<String,VILS_Application__c> vAppMap = new Map<String,VILS_Application__c>();
        List<VILS_Application__c> updateList = new List<VILS_Application__c>();
        List<Contact> insertLIstC = new List<Contact>();
        try{
            for(VILS_Application__c vApp: [Select Id,Superintendent_Name__c,Superintendent_Email__c FROM VILS_Application__c where Id IN :Trigger.newMap.keySet()]){
                cnameList.add(vApp.Superintendent_Name__c);
                emailList.add(vApp.Superintendent_Email__c);
                vAppMap.put(vApp.Superintendent_Name__c+'::'+vApp.Superintendent_Email__c,vApp);
            }

            for(Contact c:[Select Id,Name,email from Contact where Name in :cnameList or email in :emailList]){
                if(vAppMap.containsKey(c.Name+'::'+c.email)){
                    VILS_Application__c vApp= vAppMap.get(c.Name+'::'+c.email);
                    vApp.Superintendent__c =c.Id;//
                    updateList.add(vApp);
                    vAppMap.remove(c.Name+'::'+c.email);
                }
            }
            
            for(VILS_Application__c vApp:vAppMap.values()){
                Contact c = new Contact();
                List<String> nameList = vApp.Superintendent_Name__c.split(' ');
                c.FirstName=nameList[0];
                if(nameList.size()==2)
                    c.LastName=nameList[1];
                else
                    c.LastName=vApp.Superintendent_Name__c;
                //c.Name=vApp.Superintendent_Name__c;
                c.email= vApp.Superintendent_Email__c;
                insertListC.add(c);
            }

            if(insertListC.size()>0){
                insert insertListC;
            }
            for(Contact c: insertListC){
                VILS_Application__c vApp= vAppMap.get(c.Name+'::'+c.email);
                vApp.Superintendent__c =c.Id;//
                updateList.add(vApp);
                vAppMap.remove(c.Name+'::'+c.email);
            }

            if(updateList.size()>0){
                update updateList;
            }
        }catch(exception e)
        {
            system.debug(e);
        }


    }

    
}